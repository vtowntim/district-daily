
exports.like = function(req, res){
	var social = require('../models/social');

	social.like(req.param('spId'),req.param('type'),req.param('user'), req.param('name'), function(err){
		if(err){console.error(err);} else{

			social.getSocial(req.param('spId'),req.param('type'), function(err, socialData){

				res.jsonp(socialData);

			})

		}
	})

};

exports.comment = function(req, res){
	var social = require('../models/social');

	social.comment(req.param('spId'),req.param('type'),req.param('user'), req.param('name'), req.param('comment'), function(err){
		if(err){console.error(err);} else{

			social.getSocial(req.param('spId'),req.param('type'), function(err, socialData){

				res.jsonp(socialData);

			})

		}
	})

};

exports.getList = function(req, res){
    var districtDaily = require('../models/districtDaily');
    var moment = require('moment');

    var listDate = moment(req.param('date')).hour(12).minute(0).format('YYYY-MM-DD HH:mm');

    console.log(req.param('date'));
    console.log(listDate);

    districtDaily.getList(listDate, function(err,list){

    	res.jsonp(list);

    })

}


exports.getSocial = function(req, res){
	var social = require('../models/social');

	social.getSocial(req.param('spId'),req.param('type'), function(err, socialData){
		if(err){console.error(err);} else{

			res.jsonp(socialData);

		}
	})

};

exports.getContent = function(req, res){
	var quotes = require('../models/scheduledContent');

	quotes.getContent(req.param('date'), function(err, quotes){
		if(err){console.error(err);} else{

			res.jsonp(quotes);

		}
	})

};

exports.saveContent = function(req, res){
	var quotes = require('../models/scheduledContent');

	var contentObj = {
		date: req.param('date'),
		type: req.param('type'),
		subject: req.param('subject'),
		content: req.param('content'),
		meta: req.param('meta')
	}

	quotes.saveContent(contentObj, function(err, quotes){
		if(err){console.error(err);} else{

			res.jsonp({status: 'Saved!'});

		}
	});
}

exports.searchQuotes = function(req, res){
	var quotes = require('../models/scheduledContent');
	quotes.searchQuotes(req.param('term'), function(err, quotes){
		if(err){console.error(err);} else{

			res.jsonp(quotes);

		}
	});
}

exports.usernames = function(req, res){
	var kdPeople = require('../models/kdPeople');

	kdPeople.getEmployeeUsernames(function(error,usernames){
		if(usernames) res.send( JSON.stringify(usernames) );
	});

}