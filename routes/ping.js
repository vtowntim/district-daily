

var topOfTheHour = function(req, res){
    var sqlUtil = require('../models/sqlUtil');
    // Clear our the Hits table, leaving only the oldest records per user, per post.
    sqlUtil.simpleSql('delete from [districtDaily-Hits] where [date] not in ( SELECT min(date) FROM [districtDaily-Hits] group by [id],[user] )', function(err, recordset) {
        if(err){console.error(err);}
    });

}

var TenReview = function(req, res){
    var districtDaily = require('../models/districtDaily');
    districtDaily.build(function(err, output){ if (err){console.error(err);} else {

        var dailyReviewers = ['tanderso','dvolosin','ddesimas','bcapwell'];

        districtDaily.prepareAndSendTheDaily(res, dailyReviewers, function(err, content){
            if(err){console.error(err);}
        });

    }});
}
exports.TenReview = TenReview;

var NoonDeliver = function(req, res){
    var districtDaily = require('../models/districtDaily');
    districtDaily.build(function(err, output){ if (err){console.error(err);} else {

        districtDaily.prepareAndSendTheDaily(res, null, function(err, content){
            if(err){console.error(err);}
        });

    }});
}

exports.ping = function(req,res){
    var moment = require('moment');

	res.send(200);
    console.log('Ping from SSIS!');

    var job;

    // Run 'topOfTheHour' if the time is within 5 minutes of :00
    if ( moment().add('m', 5).minute() < 10 ) { job = 'Top of the hour'; }

    // Run 'Noon-Deliver' if the time is within 5 minutes of 12:00
    if ( Math.abs( moment().startOf('day').hour(12).diff(moment(),'m') ) < 15 ) { job = 'Noon-Deliver'; }

    // Run '10-Review' if the time is within 5 minutes of 10:00
    if ( Math.abs( moment().startOf('day').hour(10).diff(moment(),'m') ) < 5 ) { job = '10-Review'; }

    console.log('Running job: ' + job);

    switch(job){
    	case 'Noon-Deliver':
    		NoonDeliver(req, res);
    	break;
    	case '10-Review':
    		TenReview(req, res);
    	break;
        case 'Top of the hour':
            topOfTheHour(req, res);
        break;
    }

};