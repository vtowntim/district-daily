

exports.ping = function(myreq, myres){
    var utils = require('../models/utils');
    var moment = require('moment');

    var theIP = utils.getClientAddress(myreq);

    console.log('Ping from SSIS! - ' + moment().format() + ' - ' + theIP );

    myres.send('Ping from DDaily!')
};

exports.list = function(myreq, myres){
    var moment = require('moment');

    var today = moment().hour() >= 12 ? moment() : moment().subtract('d', 1);

    console.log(moment().hour());

    myres.redirect('/'+ today.format('YYYY/MM/DD') +'/');
}

exports.updateEvents = function(req, res){
    var events = require('../models/events');

    events.getEventsFromSharePoint(function(err){
        if(err){
            console.error(err);
        }else{
            res.send('Updated Events');
        }
    });
    
}

exports.genList = function(myreq, myres) {
    var districtDaily = require('../models/districtDaily');

    districtDaily.build(function(err, output){ if (err){console.error(err);} else {

        districtDaily.gatherContent( new Date(), function(err, content){ if(err){console.error(err);} else {

            if(err){console.error(err)}
                myres.json(content);

        }});

    }});



};

exports.announcement = function(myreq, myres){
    var districtDaily = require('../models/districtDaily');

    var thisIsATest = true;

    districtDaily.announcement(myres, thisIsATest, function(err, content){
        if(err){console.error(err);}
    });

}

exports.announcementView = function(myreq, myres){
    var districtDaily = require('../models/districtDaily');

    var md = require('marked');

    var content = {
        title: 'Announcing the District Daily',
        md: md
    }

    myres.render('announcement', content);

}


exports.emailList = function(myreq, myres){
    var districtDaily = require('../models/districtDaily');

    districtDaily.prepareAndSendTheDaily(myres, true, function(err, content){
        if(err){console.error(err);}
    });

}



exports.eventList = function(myreq, myres) {
    var events = require('../models/events');

    events.getEventsList(function(err, records) { if(err){console.error(err);}
        myres.json(records);
    });
}



exports.makeEventList = function(myreq, myres) {
    var events = require('../models/events');

    events.makeEventsList(function(err, records) { if(err){console.error(err);}
        myres.json(records);
    });
}

var renderContent = function(myreq,myres,jade,content,json){
    content.pageType = 'list';

        if (json){
            myres.send(content);
        }else{
            myres.render(jade, content, function(err, html){ if (err){console.error(err);}
                myres.send( html.replace(/\??u=\?/g, '') );
            });
        }
}




exports.getList = function(myreq, myres){
    var districtDaily = require('../models/districtDaily');
    var moment = require('moment');

    var jade = myreq.param('jade') || 'districtDaily';
    var json = myreq.param('json') ? true : false;

    var listDate = moment(myreq.params[0]).hour(10).format('YYYY-MM-DD HH:mm');

    console.log('getting list for ')
    console.log(listDate);

    districtDaily.getList(listDate, function(err,content){

        if(!content){
            //myres.send(404, 'Sorry, we cannot find that!');
            //return false;
            districtDaily.gatherContent(listDate, function(err, content){
            renderContent(myreq,myres,jade,content,json);
            })
        }else{
            renderContent(myreq,myres,jade,content,json);
        }


    })

}

exports.getUser = function(myreq, myres){
    var utils = require('../models/utils');
    var kdPeople = require('../models/kdPeople');

    kdPeople.getUser('tanderso', function(err, userData){

        myres.send( utils.encrypt(userData) );
    })

}



var renderRecord = function(pageData, myreq, myres){

    switch (pageData.verb) {
        case 'post':

            var districtDaily = require('../models/districtDaily');
            var us = require('underscore')._;

            districtDaily.getPost(pageData.post, function(err, results){ if(err){console.error(err);}

                var recordData = {
                    attachments: results.attachments,
                    list: pageData.list,
                    pageType: 'record',
                    record: results.recordset[0],
                    title: results.recordset[0].title,
                    user: pageData.theUser,
                    userData: pageData.userData,
                    post: pageData.post
                }

                districtDaily.getList(recordData.list.replace(/\//g,'-') + ' 12:00', function(err,listData){
                    // recordData.listData = listData;

                    recordData.listData = listData ? us.union(listData.records.vip, listData.records.news) : [];

                    myres.render('record', recordData);
                });

                var hits = require('../models/hits');
                hits.makeHit(pageData.post, 'Content', pageData.theUser || pageData.theIP);
            });

            break;

        case 'focus':

            var districtDaily = require('../models/districtDaily');
            var us = require('underscore')._;

            districtDaily.getFocus(pageData.post, function(err, results){ if(err){console.error(err);}

                var recordData = {
                    list: pageData.list,
                    pageType: 'focus',
                    record: results.recordset[0],
                    title: results.recordset[0].subject,
                    user: pageData.theUser,
                    userData: pageData.userData,
                    post: pageData.post
                }

                districtDaily.getList(recordData.list.replace(/\//g,'-') + ' 12:00', function(err,listData){
                    // recordData.listData = listData;

                    recordData.listData = us.union(listData.records.vip, listData.records.news)

                    myres.render('record', recordData);
                });

                var hits = require('../models/hits');
                hits.makeHit(pageData.post, 'Focus', pageData.theUser || pageData.theIP, true);
            });

            break;

        case 'event':

            var districtDaily = require('../models/districtDaily');
            var us = require('underscore')._;


            districtDaily.getEvent(pageData.post, pageData.occurrence, function(err, results){ if(err){console.error(err);}

                var recordData = {
                    list: pageData.list,
                    pageType: 'event',
                    record: results.recordset,
                    title: results.recordset.title,
                    user: pageData.theUser,
                    userData: pageData.userData,
                    post: pageData.post,
                    occurrence: pageData.occurrence
                }

                recordData.listData = [];

                myres.render('record', recordData);

                var hits = require('../models/hits');
                hits.makeHit(pageData.post, 'Events', pageData.theUser || pageData.theIP);
            });

            break;


        case 'attachments':

            var content = require('../models/content');

            content.getAttachments(pageData.post, function(err, recordset) {

                var us = require('underscore')._;

                myres.json({
                    paths: us.pluck(recordset, 'path')
                });
            });

            break;


        case 'data':
            var hits = require('../models/hits');
            var moment = require('moment');

            var postType = pageData.isEvent ? 'Events' : 'Content';

            hits.countDetail(pageData.post, postType, function(err, data){ if(err){console.error(err);} else {

                    data.forEach(function(hit,index){
                        hit.date = moment(hit.date).format('YYYY-MM-DD HH:mm:ss');
                    });

                    var us = require('underscore')._;

                    myres.render('data', {
                        title: 'Hit Data for ' + pageData.post,
                        post: pageData.post,
                        columns: us.keys(data[0]),
                        rows: data
                    });
            }});

            break;


        case 'dataDownload':
            var hits = require('../models/hits');
            var moment = require('moment');
            var csv = require('csv.js');

            var postType = pageData.isEvent ? 'Events' : 'Content';

            hits.countDetail(pageData.post, postType, function(err, data){ if(err){console.error(err);} else {

                    data.forEach(function(hit,index){
                        hit.date = moment(hit.date).format('YYYY-MM-DD HH:mm:ss');
                    });

                    myres.set('Content-Type', 'application/csv');
                    myres.set('Content-disposition', 'attachment;filename=DistrictDaily_'+post+'_data.csv');
                    myres.send( csv(data) );

            }});

            break;


    }

}


exports.getEvent = function(myreq, myres){
    getRecord(myreq, myres, 'event');
}

exports.getFocus = function(myreq,myres){
    //myres.send('Building...');
    getRecord(myreq, myres, 'focus');
}


var getRecord = function(myreq, myres, verb) {
    var utils = require('../models/utils');
    var kdPeople = require('../models/kdPeople');

    var pageData = {};

    if (verb == 'event'){
        pageData.isEvent = true;
        pageData.verb = verb;
        pageData.occurrence = myreq.params[2];
    }else if(verb == 'focus'){
        pageData.isFocus = true;
        pageData.verb = verb;
        pageData.occurrence = myreq.params[2];
    }else{
        pageData.verb = myreq.params[2] || 'post';
    }

    pageData.post = myreq.params[1];
    pageData.list = myreq.params[0];

    pageData.theIP = utils.getClientAddress(myreq);

    if ( myreq.query.u ){

        var user = utils.decrypt(myreq.query.u);

        myres.cookie('user', user);
        myres.clearCookie('userData');

        var postURLSegment = pageData.post;
        if (pageData.isEvent) postURLSegment = 'E' + postURLSegment;
        if (pageData.isFocus) postURLSegment = 'F' + postURLSegment;
        //postURLSegment = pageData.isEvent ? 'E' + pageData.post : pageData.post; // If this is an event, add an 'E' before the post id.

        myres.redirect('/'+ pageData.list + '/' + postURLSegment +'/');
        return false;
    }

    pageData.theUser = myreq.cookies.user;

    if (myreq.cookies.user && !myreq.cookies.userData){
        kdPeople.getUser(pageData.theUser, function(err, userData){
            myres.cookie('userData', utils.encrypt(userData) );
            pageData.userData = userData;
            renderRecord(pageData, myreq, myres);
        })
    }else{
        pageData.userData = myreq.cookies.userData ? utils.decrypt( myreq.cookies.userData ) : null;
        renderRecord(pageData, myreq, myres);
    }

}
exports.getRecord = getRecord;



exports.sharepointImport = function(myreq, myres) {
    var districtDaily = require('../models/districtDaily');

    districtDaily.build(function(err, output){
        if (err){
            console.error(err);
        }else{
            myres.send(output);
        }
    })

}