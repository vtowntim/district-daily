#The District Daily Style and Content Guide
The District Daily is a communication tool that captures emails broadcast to **Everyone* and transforms them into a headline list linked to individual posts. Because this is a paradigm change, this style and content guide has been created to assist Content Providers in making informed choices when submitting posts.

- - -

## Terminology
The following are definitions to terms that will be referenced throughout this document:

* **Post** — A communication document submitted to be published by the District Daily. A post contains at minimum, a *Headline* and *Content*:
	* **Headline** — The title of a post that succinctly conveys basic information and entices readers to click-through and view the post's content.
	* **Content** — The body of a post which may contain more detailed information than the headline. When a headline is clicked in the District Daily, readers are linked to read the content in a web browser. Content may contain images and attachments, and is not limited in size.
* **Content Provider** — Individuals who will submit posts (*via Email or otherwise*) to the District Daily.
* **Readers** — Individuals including all district employees who receive the District Daily.
- - -

## Posts
###Know Your Audience
In the past, **Everyone* emails went to... *well,* everyone and Content Providers didn't think much about the global relevancy of their messages. With the District Daily, we're asking everyone to think twice before posting anything at all. A click-tracking feature of the District Daily shows you how people actually read your message, allowing you to make better data-driven decisions about your posts. This isn't an effort to discourage communication, but to help make it more meaningful.

Ask yourself:

* **Does my message only apply to a certain department or smaller audience?** If so, maybe a district-wide post isn't the appropriate medium.
* **Does my message convey useful  information?** For instance, are you just telling us that your ancillary, non-patient-care department is closed for Thanksgiving? 

- - -

## Headlines
###Subject Lines Become Headlines
In the District Daily, your email subject lines become *Headlines* that would either entice or discourage readers from clicking through to your content. Before you hit “*send*,” take a moment to write a subject line that accurately and concisely describes your content, giving readers a concrete reason to engage with your message. 

### 7 Guidelines for Composing Engaging District Daily Headlines:
1. **Keep it very simple and understandable.** Do not use abbreviations, technical jargon, or hard to understand words. A well-written headline and content, should be written at a grade 7 reading level.
2. **Be precise.** Avoid writing *'Blind Headlines'* that reveal nothing about your content. A headline must stand entirely on it's own merit, without the reader being forced to read on to discover what the headline was meaning. They won't read on.
3. **Tell more; Sell more.** Headlines containing 10 or more words with factual or news worthy information, outsell short headlines and will be more successful at attracting click-throughs.
4. **Keep it between 90 and 150 characters.**  Keep your headlines short and concise. Don’t write convoluted copy that will take your visitor a long time to read. Studies have shown that the most engaging headlines are between 90 and 150 characters.
5. **Begin with *Subjects* and *Verbs*.** In the English language, we read from left to right, and verbs and subjects help us to quickly glean the meaning of a sentence. 
> *Example: 'Join us at the Kaweah Delta Talent Show - Wednesday, March 26th at 7pm'*
6. **Do not use *ALL CAPITAL LETTERS*.** Though you may think you are emphasizing your headline, it comes across as yelling. Also, because readers have learned to recognize the *'shape'* of words, a skimming reader may miss your headline altogether.
7. **Use numbers when possible.** Using data and numbers is a great way to demonstrate that your message is clear and straightforward. Numbers break through the clutter of ambiguity, adds specificity to your offer, and sets the clear expectations.
> *Example: 'The Nursing Banquet is 6.5 weeks away! Work with your Team to Donate a Basket!'*


###Do You Need More Than a Headline?
Many posts that end up in the District Daily really don't need anything more than an informative, clear headline. For instance, if you are just wanting to remind readers of an event you already posted about, *explicitly include the relevant information in your headline*.

> **Example:**
 
> * *Support Services Building: Power Outage this Friday, 7am to 8:45am*
* *Kaweah Korner will be Closed Early Monday and Tuesday at 12pm*
* *Remember to Wear Pink on Friday to Support Breast Cancer Awareness!*

###Title-Casing
A good rule of thumb for District Daily headlines is to use Title-Casing; Capitalize the first word of the headline, the last word, and all major words (*usually more than 4 letters*) in between.

> **Correct:**

> * *Support Services Building: Power Outage this Friday, 7am to 8:45am*
* *Kaweah Korner will be Closed Early Monday and Tuesday at 12pm*
* *Remember to Wear Pink on Friday to Support Breast Cancer Awareness!*

> **Incorrect:**  

> * *Support services building: power outage this friday, 7am to 8:45am*
* *Kaweah Korner Will Be Closed Early Monday And Tuesday At 12pm*
* *remember to wear pink on Friday to support Breast Cancer*

- - -

## Content
###Not an Email, a Web Page
The body of emails submitted to the District Daily are transformed into individual web pages linked from Headlines. Please recognize that web pages and emails have very different contexts and should be treated as separate mediums.

### Email Patterns to Avoid
Failure to avoid the following patterns may cause readers to perceive your content to be out-of-place or irrelevant.

* **Remove your signature.** All posts published by the District Daily have an informative *By-line* linking to your profile. Email signatures are redundant and generally take up more space than needed. If your content requires follow-up communication from readers, consider adding a '*call-to-action*' within your content explicitly prescribing your preferred contact method.
* **Remove forwarding and replying headers.** If your content is the last thread of email replies or forwards, clean it up and give readers only the information they need.
* **Remove confidentiality disclaimers.** Aside from the fact that these disclaimers are [pointless in even emails](http://www.economist.com/node/18529895), in the context of published web pages, they are nonsensical.


### Guidelines for Writing Worthwhile Content

* **Start off on a good foot.** The opening paragraph, called the '*lede*' in journalism, is often considered the most important part of an article, for the same reasons a headline is so important: if the lede isn’t good, the reader will skip the rest of the article. Get the readers' attention without being too sensational, and explain *why* they should keep reading.
* **Write short paragraphs, separated by blank lines.** Most people find unbroken blocks of text boring, or even intimidating. Take the time to format your message for the ease of your reader.
* **Use bullets.** The most effective way to present information and keep readers from leaving is to make sure that everything is compact and easy to read.
	* Using bullets will help you do that.
	* They eliminate the need for too many unnecessary words.
	* Bullets are scannable.
	* Readers who are scanning a page will easily consume the content.
* **Limit paragraphs to less than 5 sentences.** Most people don’t like reading a lengthy post. Avoid fluff and resist the urge to digress. Paragraphs that are less than 5 sentences are also easier to comprehend and readers will find it easier to recall your message when less words were used. *Less is more*.
* **Bold your main points.** If you want to emphasize a point in the middle of a longer paragraph, **you can make it bold**. Don’t do this too often or readers will tire of it.
* **Include Images** Readers are visual learners and images can help people take in and retain information better. Referencing a flier? Include it as an image.
* **Write in a conversational style.** There is a road sign often posted near construction sites that reads, "*Maintain present lane.*" Why so formal? A more conversational style would be better: "*Stay in your lane*" or "*Do not change lanes.*" If you write as if you’re wearing a top hat and spats, you distance yourself from the reader and muddle the message.