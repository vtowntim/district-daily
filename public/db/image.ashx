<%@ webhandler language="C#" class="NWEmpPhotoHandler" %>
using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
public class NWEmpPhotoHandler : IHttpHandler
{
    public bool IsReusable { get { return true; } }

    public void ProcessRequest(HttpContext ctx)
    {
        string id = ctx.Request.QueryString["id"];
        string table = ctx.Request.QueryString["t"];
        string imageColumn = ctx.Request.QueryString["ic"];
        string idColumn = ctx.Request.QueryString["idc"];

        using(SqlConnection con = new SqlConnection("Data Source=sql02;Initial Catalog=KDPeople;Persist Security Info=True;User ID=kdnet;Password=issdata")){
            SqlCommand cmd = new SqlCommand("select " + imageColumn + " from " + table + " where " + idColumn + " = @id", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@id", id);

            con.Open();
            byte[] pict = (byte[])cmd.ExecuteScalar();
            con.Close();

            ctx.Response.Cache.SetCacheability(HttpCacheability.Public);
            ctx.Response.Cache.SetExpires(DateTime.Now);
            ctx.Response.Cache.SetMaxAge(new TimeSpan(0,0,0)); 
            ctx.Response.AddHeader("Last-Modified", DateTime.Now.ToLongDateString());


            if (pict != null){
                ctx.Response.ContentType = "image/png";
                ctx.Response.OutputStream.Write(pict, 0, pict.Length - 0);
                }else{
                    ctx.Response.Redirect("../img/noPhoto.jpg", true);
                }
        }
    }
}
