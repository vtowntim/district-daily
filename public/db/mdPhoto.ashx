<%@ webhandler language="C#" class="NWEmpPhotoHandler" %>
using System;
using System.IO;
using System.Web;


public class NWEmpPhotoHandler : IHttpHandler
{
    public bool IsReusable { get { return true; } }

    public void ProcessRequest(HttpContext ctx)
    {

        string photo = ctx.Request.QueryString["photo"];

        string w = ctx.Request.QueryString["w"];

        string h = ctx.Request.QueryString["h"];

        string curFile = @"\\iis4\doctorpic\" + photo;

        //ctx.Response.Write(File.Exists(curFile) ? "File exists." : "File does not exist.");

        if (File.Exists(curFile)){
            ctx.Response.Redirect("http://kdnet.kdhcd.org/_code/asp_jpeg.asp?path=%5C%5Ciis4%5Cdoctorpic%5C" + photo + "&width=" + w + "&height=" + h, true);
            }else{
            ctx.Response.Redirect("../img/noPhoto.jpg", true);
            }

    }
}
