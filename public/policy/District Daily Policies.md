# District Daily Policies

## Terminology
The following are definitions to terms that will be referenced throughout this document:

* **Post** — A communication document submitted to be published by the District Daily. A post contains at minimum, a *Headline* and *Content*:
	* **Headline** — The title of a post that succinctly conveys basic information and entices readers to click-through and view the post's content.
	* **Content** — The body of a post which may contain more detailed information than the headline. When a headline is clicked in the District Daily, readers are linked to read the content in a web browser. Content may contain images and attachments, and is not limited in size.
* **Content Provider** — Individuals who will submit posts (*via Email or otherwise*) to the District Daily.
* **Readers** — Individuals including all district employees who receive the District Daily.
* **District Daily Subcommittee** — A group of district employees formed by the Kaweah Care Employee Connection team tasked with promoting and curating the District Daily.
- - -

## Section A - The District Daily
1. The District Daily software is set to deliver the District Daily emails every day at 12:00pm. Depending on various server conditions, recipients may receive the District Daily in their email inbox between 12:00pm and 12:15pm.
2. The District Daily software will collect emails sent to **Everyone* from 10:00am the previous day until 9:59am of the current day.
- - -

## Section B - Posts
1. Because the District Daily will begin delivering at 10:00am, all posts must be submitted by Content Providers by 9:59am. Posts submitted after 9:59am will be included in the next day's publication.
2. Posts are to be submitted via email *(to *Everyone)* or through future methods made available by the District Daily Subcommittee.
3. Content Providers should review and make an effort to conform to the *District Daily Style and Content Guide*'s guidance on Posts.
4. Posts submitted to the District Daily should:

	I. Not include any Protected Health Information or any other sensitive information that may pose risks to the security, legal compliance, or integrity of the organization.
	
	II. Be relevant and useful to a majority of District Employees.
	
	III. Make proper use of any categorization systems available.
5. Posts found by the District Daily Subcommittee to not comply with these policies or grossly disregard the guidance of the *District Daily Style and Content Guide* may be subject to alteration or complete removal from the District Daily publication.
	
- - -
	
## 	Section C - Headlines
1. Content Providers should review and make an effort to conform to the *District Daily Style and Content Guide*'s guidance on Headlines.
2. Headlines of posts submitted via email are composed in the email's subject line.
3. Headlines submitted to the District Daily should:
		
	I. Be concise, precise, and understandable.
	
	II. Not include email prefixes *(such as 'Fwd:', 'Re:', etc.)*
	
	III. Not use ALL CAPS inappropriately. *(Appropriate uses may include, but are not limited to abbreviations and initialisms.)*
	
	IV. Not intentionally mislead readers about the content of the post. All headlines must respect the integrity of the publication by not seeming to *'play games'* with readers in order to increase a post's click-through count.

4. Headlines found by the District Daily Subcommittee to not comply with these policies or grossly disregard the guidance of the *District Daily Style and Content Guide* may be subject to alteration or complete removal from the District Daily publication.

- - -

## Section D - Content
1. Content Providers should review and make an effort to conform to the *District Daily Style and Content Guide*'s guidance on Content.
2. Content of posts submitted via email are composed in the email's body.
3. Content submitted to the District Daily should:
	
	I. Not include the signature lines of the Content Provider.
	
	II. Not include forwarding and replying email headers.
	
	III. Not include confidentiality disclaimers.
4. Content Providers may elect to exclude Content from their post altogether providing that the Headline conveys enough information stand alone.
5. Content found by the District Daily Subcommittee to not comply with these policies or grossly disregard the guidance of the *District Daily Style and Content Guide* may be subject to alteration or complete removal from the District Daily publication.                                                                                                                                                                                                                                    