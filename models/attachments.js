var async = require('async');
var curl = require('curling');
var sql = require('mssql');
var sqlUtil = require('../models/sqlUtil');
var us = require('underscore')._;
var utils = require('../models/utils');

var processAttachments = function(itemId, attachments) {
    attachments = us.compact(attachments);

    async.each(attachments, function(item, callback) {

        if (item.indexOf('OriginalMessage.eml') + 1) {
            callback();
            return false;
        }

        insertAttachment(itemId, item, callback);
        saveAttachment(item);

    }, function(err) {
        if (err) {
            console.error(err);
        }

    })


}
exports.processAttachments = processAttachments;

var insertAttachment = function(itemId, path, callback) {
    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);
        request.input('spId', sql.Int, itemId);
        request.input('path', sql.VarChar, utils.newPathName(path));
        request.execute('districtDailyInsertAttachments', function(err, recordsets, returnValue) {
            // ... error checks
            if (err) {
                console.error(err);
            }
            if (callback) {
                callback();
            }

            // ...
        });
    });
}
exports.insertAttachment = insertAttachment;

var saveAttachment = function(path, callback) {
    var newPath = APPDIR + '\\public\\attachments\\' + utils.newPathName(path);
    var command = '--GET -o "' + newPath + '" --ntlm -u kdhcd/timapp:timapp "' + encodeURI(path) + '"';

    curl.run(command, function(err, result) {
        if (err) {
            console.error(err);
        }
        if (callback) {
            callback();
        }
    });
}
exports.saveAttachment = saveAttachment;


var getAttachments = function(itemId) {
    var url = 'http://daily.kdhcd.org/public/db/db.aspx?mode=getAttachments&id=' + itemId;

    http.get(url, function(res) {
        var body = '';

        res.on('data', function(chunk) {
            body += chunk;
        });

        res.on('end', function() {
            var rows = JSON.parse(body)['Attachments']['Attachment'];

            if (typeof rows == 'string') {
                return false;
            }

            var attachments = [];

            async.each(rows, function(item, callback) {

                if (item.indexOf('OriginalMessage.eml') + 1) {
                    callback();
                    return false;
                }

                //console.log( itemId + ' - ' + item);
                insertAttachment(itemId, item, callback);
                saveAttachment(item);

            }, function(err) {
                if (err) {
                    console.log(err);
                }

            })

        });
    }).on('error', function(e) {
        console.log("Got error: ", e);
    });
};
exports.getAttachments = getAttachments;
