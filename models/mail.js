var moment = require('moment');
var nodemailer = require("nodemailer");

var makeTransport = function() {
    return nodemailer.createTransport("SMTP", {
        host: "exchange03.kdhcd.org", // hostname
    });
}

var sendMail = function(mailOptions, callback) {
    makeTransport().sendMail(mailOptions, callback);
    console.log('Sending mail to ' + mailOptions.to );
}
exports.sendMail = sendMail;



var errorMail = function(message){
    var mailOptions = {
        from: "District Daily <DistrictDaily@kdhcd.org>",
        to: 'tanderso@kdhcd.org',
        subject: "District Daily Error - " + moment().format('llll'),
        text: message
    }
    sendMail(mailOptions);
}
exports.errorMail = errorMail;



var blast = function(mailOptions, callback){
    var utils = require('../models/utils');
    
    if (typeof mailOptions.to === 'string'){ mailOptions.to = mailOptions.to.split(','); } // turn the to to an array if it isn't already...

    var myHTML = mailOptions.html;

    mailOptions.to.forEach(function(username, index){

        if (username){
            mailOptions.to = username + '@kdhcd.org';
            mailOptions.html = myHTML.replace(/u=\?/g, 'u=' + utils.encrypt(username) );
            sendMail(mailOptions, callback);
        }


    });

}
exports.blast = blast;