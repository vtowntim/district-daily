var async = require('async');
var moment = require('moment');
var sp = require('../models/sp');
var sql = require('mssql');
var sqlUtil = require('../models/sqlUtil');
var us = require('underscore')._;
var utils = require('../models/utils');
var xml2js = require('xml2js').parseString;

var noonToday = moment().set('h', 12).set('m', 0).set('s', 0);

var startOfMonth = moment(noonToday).startOf('month');

var nextWeek = moment().set('h', 12).set('m', 0).set('s', 0).add('w', 1);

var spWeekdayMap = {
    su: 0,
    mo: 1,
    tu: 2,
    we: 3,
    th: 4,
    fr: 5,
    sa: 6
};

var stMap = {
    first: 1,
    second: 2,
    third: 3,
    fourth: 4,
    fifth: 5
}

var getEventsList = function(callback) {

    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('startDate', sql.DateTime, noonToday.toDate());

        if(!callback) callback = function(){};

        request.execute('districtDaily-getEventWeek', function(err, recordset){

            recordset = recordset[0];

            //Expand the JSON-encoded recurrence field:
            recordset.forEach(function(record, k) {
                if (record.recurrence != null) {
                    record.recurrence = JSON.parse(record.recurrence);
                }
             })

            callback(err, recordset);

        });
    });


}
exports.getEventsList = getEventsList;

var weekdayOfMonth = function(st, weekday, context) { // first, 4 (thurs), 2014-04-28
    var startOfMonth = moment(context).startOf('month');
    var adjust = startOfMonth.day() <= weekday ? -1 : 0;
    adjust += stMap[st];
    var adjDate = moment(startOfMonth).day(weekday + (adjust * 7));
    // console.log('WeekdayOfMonth')
    // console.log(adjDate);
    return adjDate;
}

var meetsMonthFrequency = function(context, freq, startDate) {
    var response = (moment(context).diff(moment(startDate), 'months') % freq) ? false : true;
    // console.log('Meets Month Frequency?');
    // console.log(context.toDate());
    // console.log(freq);
    // console.log(startDate);
    // console.log(response);
    return response;
}

var isInDateRange = function(context, startDate, endDate) {
    var response = (moment(context).isAfter(startDate) && moment(context).isBefore(endDate)) ? true : false;
    // console.log('Is In Date Range?');
    // console.log(context.toDate());
    // console.log(startDate.toDate());
    // console.log(endDate.toDate());

    // console.log(response);
    return response;
}

var evaluateRecurrence = function(currentRecord, dateWindowFrom, dateWindowTo){

    // console.log('FINDING RECURRENCES');
    // console.log( JSON.stringify(currentRecord) );

    var startTime = moment(currentRecord.record.startDate).format('HH:mm');
    var endTime = moment(currentRecord.record.endDate).format('HH:mm');

    var repeat = currentRecord.record.recurrence.rule[0].repeat[0];
    currentRecord.recRule = us.keys(repeat)[0];
    currentRecord.ruleDetail = repeat[currentRecord.recRule][0]['$'];
    var ruleWeekDay = spWeekdayMap[us.keys(currentRecord.ruleDetail)[0]];
    var windowEnd = currentRecord.record.recurrence.rule[0].windowEnd ? currentRecord.record.recurrence.rule[0].windowEnd[0] : currentRecord.record.endDate;

    var evaluatedRecord;


    switch (currentRecord.recRule) {
        case 'monthlyByDay':

            // console.log('weekdayOfMonth')
            // console.log(currentRecord.ruleDetail.weekdayOfMonth);
            // console.log('ruleWeekDay')
            // console.log(ruleWeekDay);

            //Get Occurrences for this month and next month... -- for example, thisMonth == the second thursday of this month -- nextMonth == the second thursday of next month
            var thisMonth = weekdayOfMonth(currentRecord.ruleDetail.weekdayOfMonth, ruleWeekDay, dateWindowFrom);
            var nextMonth = weekdayOfMonth(currentRecord.ruleDetail.weekdayOfMonth, ruleWeekDay, moment(dateWindowFrom).add('month', 1));

            var thisMonth = moment(thisMonth.format('YYYY-MM-DD ') + startTime);
            var nextMonth = moment(nextMonth.format('YYYY-MM-DD ') + startTime);


            if (isInDateRange(thisMonth, dateWindowFrom, dateWindowTo) && meetsMonthFrequency(thisMonth, currentRecord.ruleDetail.monthFrequency, currentRecord.record.startDate)) {
                // Is this month's occurrence within the date range? (the next two weeks) ..and does it meet the monthFrequency?
                currentRecord.start = thisMonth.format('YYYY-MM-DD HH:mm')
                currentRecord.end = thisMonth.format('YYYY-MM-DD ') + endTime;
                evaluatedRecord = us.clone(currentRecord);
                }
            else if (isInDateRange(nextMonth, dateWindowFrom, dateWindowTo) && meetsMonthFrequency(nextMonth, currentRecord.ruleDetail.monthFrequency, currentRecord.record.startDate)) {
                // Is next month's occurrence within the date range? (the next two weeks) ..and does it meet the monthFrequency?
                currentRecord.start = nextMonth.format('YYYY-MM-DD HH:mm');
                currentRecord.end = nextMonth.format('YYYY-MM-DD ') + endTime;
                evaluatedRecord = us.clone(currentRecord);
                }

            break;
        case 'weekly':

            var adjust = moment(dateWindowFrom) < ruleWeekDay ? -1 : 0;
            var dayFormulas = [
                ruleWeekDay + (adjust + 0 * 7),
                ruleWeekDay + (adjust + 1 * 7),
                ruleWeekDay + (adjust + 2 * 7)
                ];

            dayFormulas.forEach(function(dayFormula, index){
                var day = moment(dateWindowFrom).day(dayFormula);

                if( isInDateRange(day, dateWindowFrom, dateWindowTo) ){
                    currentRecord.start = day.format('YYYY-MM-DD ') + startTime;
                    currentRecord.end = day.format('YYYY-MM-DD ') + endTime;
                    evaluatedRecord = us.clone(currentRecord);
                }
            })

            break;
        case 'daily':

            var laterStartDate = moment(dateWindowFrom).isAfter(currentRecord.start) ? moment(dateWindowFrom) : moment(currentRecord.start);
            var soonerEndDate = moment(windowEnd).isBefore(dateWindowTo) ? moment(windowEnd) : moment(dateWindowTo);

            for (var i=0; moment(laterStartDate).add(i,'days').isBefore(soonerEndDate) ;i++){ 

                var day = moment(laterStartDate).add(i,'days');
                // console.log(day.format());

                if( isInDateRange(day, dateWindowFrom, dateWindowTo) ){
                    currentRecord.start = day.format('YYYY-MM-DD ') + startTime;
                    currentRecord.end = day.format('YYYY-MM-DD ') + endTime;
                    evaluatedRecord = us.clone(currentRecord);
                    }

                }

            break;
    }

    // console.log(evaluatedRecord)

    return evaluatedRecord;

};
exports.evaluateRecurrence = evaluateRecurrence;



var makeOccurrence = function(record, dateWindowFrom, dateWindowTo){

    // console.log('making occurrence');
    // console.log(record);

    var startDate = moment(record.startDate),
        endDate = moment(record.endDate); // turn the start and end dates to moments()

    endDate = endDate.isBefore(dateWindowTo) ? endDate : dateWindowTo;

    // cr == Current Record
    var currentRecord = {
        start: record.startDate,
        end: record.endDate,
        record: record
    }

    var theOccurance;

    if (!record.recurrence){

        theOccurance = us.clone(currentRecord);

    }else{

        theOccurance = evaluateRecurrence(currentRecord, dateWindowFrom, dateWindowTo);

    }

    if (theOccurance){
        theOccurance.occurrence = moment(theOccurance.start).format('YYYY.DDD') ;
        return theOccurance;
    }else{
        return null;
    }

};
exports.makeOccurrence = makeOccurrence;



var makeEventsList = function(callback) {

    getEventsList(function(err, recordset) {

        if (err) {

            callback(err);

        } else {

            var thisWeek = [];

            recordset.forEach(function(record, k) { //Iterate thru each returned event

                var occurrence = makeOccurrence(record, noonToday, nextWeek); //see if the event has any occurances within the next week
                if (occurrence) thisWeek.push( occurrence ); //if there are occurances, add it to the array

            });

            // console.log('THIS WEEK ARRAY:');
            // console.log(thisWeek);

            thisWeek.sort(function(a,b){
                if( moment(a.start).isBefore(b.start) ) return -1;
                if( moment(b.start).isBefore(a.start) ) return 1;
                return a.record.title - b.record.title;
            });

            var thisWeekGrouped = {};

            thisWeek.forEach(function(event,index){

                var myDay = moment(event.start).format('MMM DD YYYY');

                if(! thisWeekGrouped[myDay] ){
                    thisWeekGrouped[myDay] = [];
                    }

                thisWeekGrouped[myDay].push(event);
            });

            callback(err, thisWeekGrouped);

        }

    });

};
exports.makeEventsList = makeEventsList;



var insertEvent = function(row, callback) {

    if (!row) {
        return false;
    }

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        // recurrence data - @ows_RecurrenceData

        request.input('spId', sql.Int, row['@ows_ID']);
        request.input('body', sql.Text, utils.cleanHTML(row['@ows_Description']));
        request.input('creator', sql.VarChar, row['@ows_Author'].match(/\#(.*)/)[1]);
        request.input('username', sql.VarChar, null);
        request.input('startDate', sql.DateTime, new Date(row['@ows_EventDate']));
        request.input('endDate', sql.DateTime, new Date(row['@ows_EndDate']));
        request.input('title', sql.VarChar, row['@ows_Title'].replace(/\s?\#(\w*)\s?/g, '').replace(/^-\s*|\s$/,''));
        request.input('allDay', sql.Bit, row['@ows_fAllDayEvent'] == "1");
        request.input('recurring', sql.Bit, row['@ows_fRecurrence'] == "1");
        request.input('location', sql.VarChar, row['@ows_Location']);
        request.input('category', sql.VarChar, row['@ows_Category'] ? row['@ows_Category'] : null);
        request.input('eventStyle', sql.VarChar, row['@ows_Event_x0020_Style'] != "None Selected" ? row['@ows_Event_x0020_Style'] : null);


        var tags = row['@ows_Title'].match(/\#(\w*)/g, '');
        tags = tags ? tags.join() : null;
        request.input('tags', sql.VarChar, tags);

        var seriesTasks = {

            jsonifyRecurrenceXML: function(callback) {
                if (!row['@ows_RecurrenceData']) {
                    callback(null);
                    return false;
                }
                xml2js(row['@ows_RecurrenceData'], function(err, js) {
                    if (err) {
                        callback(err)
                    } else {
                        js = js.recurrence;

                        request.input('recurrence', sql.Text, JSON.stringify(js));
                        callback(null);
                    }
                });
            }

        };

        // console.log(request);

        async.series(seriesTasks, function(err, results) {

            request.execute('districtDailyInsertEvent', function(err, recordsets, returnValue) {
                console.log('Inserted/Updated EVENT #' + row['@ows_ID']);
                callback(err);
            });

        })


    });
}
exports.insertEvent = insertEvent;


var getEvent = function(event, occurrence, callback) {
    var sqlUtil = require('../models/sqlUtil');
    sqlUtil.simpleSql("select top 1 * from [dbo].[districtDaily-Events] where spId = '" + event + "' order by id desc", function(err, recordset) {

        var theEvent = recordset[0];

        ///TODO: Parse the occurrence parameter to call the makeOccurrence function and return the correct occurence


        callback(err, theEvent);
    })
}
exports.getEvent = getEvent;


var getEventsFromSharePoint = function(callback) {

    var url = 'http://daily.kdhcd.org/public/db/db.aspx?mode=getEvents';

    sp.getSPList(url, function(err, rows) {

        if (err) {
            callback(err);
            return false;
        }

        if (rows){
            console.log('got event')
            console.log( JSON.stringify(rows) );
            if (!Array.isArray(rows)){
                rows = [rows];
            }
            async.each(rows, function(row, myCallback) {

                // console.log(row); 
                insertEvent(row, function(err) {
                    myCallback(err);
                });
            }, function(err) {
                callback(err);
            });
        }else{
            callback(err);
        }

    });

}
exports.getEventsFromSharePoint = getEventsFromSharePoint;

