

var getSPList = function(url, callback) {
    var http = require('http');

    console.log('Getting ' + url)

    http.get(url, function(res) {
        var body = '';
        //console.log(url);

        res.on('data', function(chunk) {
            body += chunk;
        });

        res.on('end', function() {
            //console.log(body)
            body = JSON.parse(body)['listitems']['rs:data']

            if (body['@ItemCount'] != '0'){
                var rows = body['z:row'];
            }else{
                var rows = null;
            }

            callback(null, rows);

        }).on('error', function(err) {
            callback(err, null);
        });

    });
}
exports.getSPList = getSPList;