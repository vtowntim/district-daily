

var searchQuotes = function(term, callback){

	var sql = require('mssql');
	var sqlUtil = require('../models/sqlUtil');

	var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('term', sql.NVarChar, term);

        if(!callback) callback = function(){};

        request.execute('districtDaily-quotesSearch', function(err, quotes){
        	callback(err, quotes[0] );
        });
    });

}
exports.searchQuotes = searchQuotes;


var getContent = function(date, callback){

    console.log(date);

    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('date', sql.DateTime, new Date(date) );

        if(!callback) callback = function(){};

        request.execute('districtDaily-ScheduledGet', function(err, quotes){
            callback(err, quotes[0] );
        });
    });

}
exports.getContent = getContent;


var saveContent = function(contentObj, callback){

    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        console.log(new Date(contentObj.date));

        request.input('date', sql.DateTime, new Date(contentObj.date));
        request.input('type', sql.NVarChar, contentObj.type);
        request.input('subject', sql.NVarChar, contentObj.subject);
        request.input('content', sql.NText, contentObj.content);
        request.input('meta', sql.NVarChar, contentObj.meta);

        if(!callback) callback = function(){};

        request.execute('districtDaily-ScheduledInsert', function(err, data){
            callback(err, data[0] );
        });
    });

}
exports.saveContent = saveContent;