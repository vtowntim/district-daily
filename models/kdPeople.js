
var getUser = function(username, callback){
	var sql = require('mssql');
	var sqlUtil = require('../models/sqlUtil');

	var connection = new sql.Connection(sqlUtil.getConfig('KDPeople'), function(err) {
        var request = new sql.Request(connection);

        request.input('username', sql.VarChar, username);

        if(!callback) callback = function(){};

        request.execute('employee.spGetEmployeeByUsername', function(err, userData){
        	callback(err, userData[0][0]);
        });
    });
}
exports.getUser = getUser;


var getEmployeeUsernames = function(callback){
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');
    var us = require("underscore");

    var connection = new sql.Connection(sqlUtil.getConfig('KDPeople'), function(err) {
        var request = new sql.Request(connection);

        if(!callback) callback = function(){};

        request.execute('employee.spDistrictDailyUsernames', function(err, usernames){
            callback(err, us.pluck(usernames[0],'NetworkID') );
        });
    });
}
exports.getEmployeeUsernames = getEmployeeUsernames;