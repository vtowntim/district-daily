


var insertList = function(date, content, callback){
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');
    var moment = require('moment');

    if (!date || !content) {
        return false;
    }

    if (typeof content == 'object'){
        contentObject = JSON.stringify(content);
    }else{
        contentObject = content;
    }

    console.log('Inserting content for');
    console.log(moment(date).toDate());

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('date', sql.DateTime, moment(date).toDate());
        request.input('contentObject', sql.Text, contentObject);

        request.execute('districtDailyInsertList', function(err, recordsets, returnValue) {
            if(callback) {callback(err);}
            console.log('Inserted/Updated List for ' + date);
        });
    });
}
exports.insertList = insertList;

var getList = function(date, callback){
    var sqlUtil = require('../models/sqlUtil');

    sqlUtil.simpleSql("select top 1 * from [dbo].[districtDaily-Lists] where convert(date, [date]) = convert(date,'" + date + "') order by date desc", function(err, recordset) {
        var contentObject = recordset.length ? JSON.parse(recordset[0].contentObject) : null;
        callback(err,contentObject);
    });
}
exports.getList = getList;
 

var gatherContent = function(day, callback){
    var async = require('async');
    var content = require('../models/content');
    var events = require('../models/events');
    var moment = require('moment');
    var weather = require('../models/weather');

    day = day || new Date();
    var today = moment(day).hour(10).minute(0).second(0).format('YYYY-MM-DD HH:mm:ss');

    console.log('making content for')
    console.log(today)

    var processes = {

        records: function(callback){
            content.makeRecordsList(today, function(err, records){
                callback(err, records);
            });
            },

        focus: function(callback){
            content.getFocusByDay(today, function(err, focusData){
                callback(err, focusData[0][0]);
            });
            },

        weather: function(callback){
            weather.getForecast(function(err, weather){
                callback(err, weather);
            });
            },

        events: function(callback){
            events.makeEventsList(function(err, events) {
                callback(err, events);
            });
            }

    };

    async.parallel(processes,
        function(err, content){

            content.listDay = moment(today).format('YYYY-MM-DD HH:mm');

            content.title = 'District Daily for ' + moment(today).format('MMMM Do, YYYY');

            insertList(today, content);

            callback(err, content);
        });


}
exports.gatherContent = gatherContent;


var build = function(callback){
// Downloads content from SharePoint
    var async = require('async');
    var content = require('../models/content');
    var events = require('../models/events');

    var output = '';

    var processes = [

        function(callback) {
            content.getContentFromSharePoint(function(err, res) {
                output += 'Content loaded from SharePoint.\n';
                callback(err, true);
            })
        },

        function(callback) {
            events.getEventsFromSharePoint(function(err, res) {
                output += 'Events loaded from SharePoint.\n';
                callback(err, true);
            })
        }
    ];

    async.parallel(processes, function(err, results) {

        //console.log(output);

        if (callback) { callback(err, output) }
    })

}
exports.build = build;


var getPost = function(post, callback){
    var async = require('async');
    var content = require('../models/content');

    var processes = {

        recordset: function(callback) {
            content.getPost(post, callback);
        },

        attachments: function(callback) {
            content.getAttachments(post, callback);
        }

    }

    async.parallel(processes, callback);
}
exports.getPost = getPost;


var getFocus = function(focusID, callback){
    var async = require('async');
    var content = require('../models/content');

    var processes = {

        recordset: function(callback) {
            content.getFocus(focusID, callback);
        }

    }

    async.parallel(processes, callback);
}
exports.getFocus = getFocus;


var getEvent = function(event, occurrence, callback){
    var async = require('async');
    var events = require('../models/events');

    var processes = {

        recordset: function(callback) {
            events.getEvent(event, occurrence, callback);
        }

    }

    async.parallel(processes, callback);
}
exports.getEvent = getEvent;


var announcement = function(httpResponse, thisIsATest, callback){
    var md = require('marked');

    var content = {
        title: 'Announcing the District Daily',
        md: md
    }

    httpResponse.render('announcement', content, function(err, html) {

        sendTheDaily(html, httpResponse, thisIsATest, 'Announcing the District Daily!');
        if (callback){callback(err,html);}

    });
}
exports.announcement = announcement;



var prepareAndSendTheDaily = function(httpResponse, thisIsATest, callback){

    gatherContent(null, function(err, content){

        if(err){console.error(err);} else {

            httpResponse.render('mailList', content, function(err, html) {

                sendTheDaily(html, httpResponse, thisIsATest);
                if (callback){callback(err,html);}

            });
        }
    });

}
exports.prepareAndSendTheDaily = prepareAndSendTheDaily;

var blastTheDailyEmail = function(addresses, emailBodyHTML, httpResponse, subject){
    var mail = require('../models/mail');

    var mailOptions = {
        from: "District Daily <DistrictDaily@kdhcd.org>",
        to: addresses,
        subject: subject,
        html: emailBodyHTML,
        forceEmbeddedImages: false
        //generateTextFromHTML: true
    }

    httpResponse.send('sent!');
    mail.blast(mailOptions);
}

var sendTheDaily = function(emailBodyHTML, httpResponse, thisIsATest, subject){
    var moment = require('moment');
    var kdPeople = require('../models/kdPeople');

    subject = subject || "District Daily - " + moment().format('dddd, MMMM Do, YYYY');

    if (thisIsATest){
        console.log('Sending the District Daily using Test Addresses.');
    }else{
        console.log('Sending the District Daily');
    }


    if (thisIsATest){
        console.log('This is a Test!')
        // var addresses = thisIsATest ? ['tanderso','dvolosin','ddesimas','bcapwell'] : [ 'tanderso', 'cmajor', 'ldryden', 'MSutherl', 'slarkin', 'cstanley', 'ddesimas', 'dvolosin', 'dquesnoy',  'bcapwell', 'mblanken','dcochran','tmendenh','wheather','kbentson','rlauck','lledesma','rlizardo','emiller','alrodrig','vturner','lflorez','GAhTye','baguilar','LSchultz','CLaBlue','CLawry','JMcNulty','dlmyers','TRayner','JRamirez','JStockto','jtyndal' ];
        var addresses = typeof thisIsATest === 'object' ? thisIsATest : 'tanderso';
        blastTheDailyEmail(addresses, emailBodyHTML, httpResponse, subject);

    }else{

        //BETA GROUP:
        // var addresses = [ 'tanderso', 'cmajor', 'ldryden', 'MSutherl', 'slarkin', 'cstanley', 'ddesimas', 'dvolosin', 'dquesnoy',  'bcapwell', 'mblanken','dcochran','tmendenh','wheather','kbentson','rlauck','lledesma','rlizardo','emiller','alrodrig','vturner','lflorez','GAhTye','baguilar','LSchultz','CLaBlue','CLawry','JMcNulty','dlmyers','TRayner','JRamirez','JStockto','jtyndal' ];
        // blastTheDailyEmail(addresses, emailBodyHTML, httpResponse, subject);

        // Production Group:
        kdPeople.getEmployeeUsernames(function(err,usernames){
            if(err){
                console.error(err);
            }
            if(usernames) {
                console.log('We have usernames!')
                blastTheDailyEmail(usernames, emailBodyHTML, httpResponse, subject);
            }
        });

    }


}
exports.sendTheDaily = sendTheDaily;