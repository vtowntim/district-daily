
var insertContent = function(row, callback) {
    var sql = require('mssql');
    var attachments = require('../models/attachments');
    var sqlUtil = require('../models/sqlUtil');
    var utils = require('../models/utils');

    if (!row) {
        return false;
    }

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        attachments.processAttachments(row['@ows_ID'], row['@ows_Attachments'].split(';#'));

        request.input('spId', sql.Int, row['@ows_ID']);
        request.input('body', sql.Text, utils.cleanHTML(row['@ows_Body']));
        request.input('creator', sql.VarChar, row['@ows_EmailFrom'].match(/(.*) \</)[1]);
        request.input('username', sql.VarChar, row['@ows_EmailFrom'].match(/<(.*)\@/)[1]);
        request.input('created', sql.DateTime, new Date(row['@ows_Created']));

        var rowTitle = row['@ows_Title'] || 'untitled';

        request.input('title', sql.VarChar, rowTitle.replace(/\s?(\#(\w*)|^RE:|^FW:|^Recall:)\s?/gi, ''));

        var tags = rowTitle.match(/\#(\w*)/g, '');
        tags = tags ? tags.join() : null;

        request.input('tags', sql.VarChar, tags);

        request.execute('districtDailyInsertContent', function(err, recordsets, returnValue) {
            callback(err);
            if(err){console.error(err);}
            console.log('Inserted/Updated #' + row['@ows_ID']);
        });
    });
}
exports.insertContent = insertContent;

var getFocusByDay = function(day, callback){
    var moment = require('moment');
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var myDate = moment(day).toDate(); //.format('YYYY-MM-DD HH:mm:ss')

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('searchDate', sql.DateTime, myDate);

        if(!callback) callback = function(){};

        request.execute('districtDaily-getFocus', callback);
    });

}
exports.getFocusByDay = getFocusByDay;

var makeRecordsList = function(day, callback) {
    var moment = require('moment');
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var date = moment(day)//.format('YYYY-MM-DD HH:mm:ss');

    var dayBefore = moment(date).subtract('day', 1).set('h', 10).set('m', 0).set('s', 0).format('YYYY-MM-DD HH:mm:ss');

    console.log('makeRecordsList');
    console.log(date.toDate());
    console.log(dayBefore);

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('created', sql.DateTime, date.toDate());
        request.execute('districtDaily-getList',
            // sqlUtil.simpleSql('select * from [dbo].[districtDaily-List] where created >= \'' + dayBefore + '\' and created < \'' + date + '\' order by created asc',
            function(err, recordset) {

                recordset = recordset[0];

                if (err) {
                    callback(err);
                } else {

                    var records = {
                        news: [],
                        vip: [],
                        garageSale: [],
                        reject: []
                    }

                    recordset.forEach(function(record, k) {

                        var bucket = records.news; // Default bucket

                        if (record.tags) {
                            bucket = (record.tags.toLowerCase().indexOf('garageSale') + 1) ? records.garageSale : bucket;
                            bucket = (record.tags.toLowerCase().indexOf('vip') + 1) ? records.vip : bucket;
                        }

                        bucket = (record.title.toLowerCase().indexOf('recall:') + 1) ? records.reject : bucket;
                        bucket.push(record);

                    })

                    callback(err, records);
                }

            });
        });

}
exports.makeRecordsList = makeRecordsList;

var getPost = function(post, callback) {
    var sqlUtil = require('../models/sqlUtil');
    sqlUtil.simpleSql("select * from [dbo].[districtDaily-Content] where spId = '" + post + "'", function(err, recordset) {
        callback(err, recordset);
    })
}
exports.getPost = getPost;

var getFocus = function(post, callback) {
    var sqlUtil = require('../models/sqlUtil');
    sqlUtil.simpleSql("select * from [dbo].[districtDaily-Scheduled] where id = '" + post + "'", function(err, recordset) {
        callback(err, recordset);
    })
}
exports.getFocus = getFocus;

var getAttachments = function(post, callback) {
    var sqlUtil = require('../models/sqlUtil');
    sqlUtil.simpleSql("select path from [dbo].[districtDaily-Attachments] where spId = '" + post + "' order by path ", function(err, recordset) {
        callback(err, recordset);
    })
}
exports.getAttachments = getAttachments;

var getContentFromSharePoint = function(callback) {
    var sp = require('../models/sp');
    var async = require('async');

    var url = 'http://daily.kdhcd.org/public/db/db.aspx?mode=getList';

    sp.getSPList(url, function(err, rows) {

        if (err) {
            console.error(err);
            callback(err);
            return false;
        } else {
            //console.log(rows)

            async.each(rows, function(row, myCallback) {
                insertContent(row, function(err) {
                    myCallback(err);
                });

            }, function(err) {
                callback(err);
            });
        }
    });

}
exports.getContentFromSharePoint = getContentFromSharePoint;

