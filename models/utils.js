

var newPathName = function(path) {
    return path
        .replace(/(.*\/Attachments\/)(.*)\/(.*)/, '$2_$3');
}
exports.newPathName = newPathName;



var getClientAddress = function(req) {
    return (req.headers['x-forwarded-for'] || '').split(',')[0].split(':')[0] || req.connection.remoteAddress.split(':')[0];
}
exports.getClientAddress = getClientAddress;

var encrypt = function(text){
    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc','d6F3Efeq')

    text = JSON.stringify(text);

    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
}
exports.encrypt = encrypt;

var decrypt = function(text){
    var crypto = require('crypto');
    var decipher = crypto.createDecipher('aes-256-cbc','d6F3Efeq')
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');

    if (dec.substring(0,1) == '"' || dec.substring(0,1) == '{' || dec.substring(0,1) == '[' ){
        dec = JSON.parse(dec);
    }

    return dec;
}
exports.decrypt = decrypt;

var cleanHTML = function(dirtyHTML) {
    var sanitizeHtml = require('sanitize-html');

    if (!dirtyHTML) {
        return null;
    }

    dirtyHTML
        .replace(/&quot;/g, '"');

    var options = {
        allowedAttributes: {
            a: ['href', 'style'],
            p: ['stlye', 'class'],
            img: ['src', 'style'],
            span: ['style'],
            div: ['style']
        },
        allowedTags: sanitizeHtml.defaults.allowedTags.concat(['img']),
        transformTags: {
            'body': sanitizeHtml.simpleTransform('div', {
                class: 'body'
            })
        },
        selfClosing: []
    }

    var clean = sanitizeHtml(dirtyHTML, options);

    return clean
        .replace(/\>(\d\.)\W+/g, '><span class="number">$1</span>')
        .replace(/\_{5,}/g, '<hr/>')
        .replace(/(<br><\/br>)/g, '<br/>')
        .replace(/\/Lists\/KDNow\/Attachments\/(\d*)\/([\w\s\%\&\.\?\=\-]*)/g, '/public/attachments/$1_$2')
        .replace(/\·\W{1,}/g, '<span class="bullet">·</span>')
        .replace(/([\r\n]){3,}/g, '$1');
}
exports.cleanHTML = cleanHTML;

