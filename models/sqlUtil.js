
var getConfig = function(database) {
    switch (database) {
        case 'Forms':
            return {
                user: 'apps',
                password: 'apps',
                server: 'sql02.kdhcd.org',
                database: 'Forms'
            };
            break;
        case 'KDPeople':
            return{
                user: 'kdnet',
                password: 'issdata',
                server: 'sql02.kdhcd.org',
                database: 'KDPeople'
            };
    }
}
exports.getConfig = getConfig;

exports.simpleSql = function(statement, callback) {
    var sql = require('mssql');
    var connection = new sql.Connection(getConfig('Forms'), function(err) {
        var request = new sql.Request(connection); // or: var request = connection.request();
        request.query(statement, function(err, recordset) {
            if (callback) {
                callback(err, recordset);
            }
        });
    });
}