

var like = function(spId, type, user, name, callback) {
	var sql = require('mssql');
	var sqlUtil = require('../models/sqlUtil');

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('spId', sql.Int, spId);
        request.input('type', sql.VarChar, type);
        request.input('user', sql.VarChar, user);
        request.input('name', sql.VarChar, name);

        if(!callback) callback = function(){};

        request.execute('districtDaily-Like', callback);
    });
}
exports.like = like;

var comment = function(spId, type, user, name, comment, callback) {
    var sql = require('mssql');
    var sqlUtil = require('../models/sqlUtil');

    var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('spId', sql.Int, spId);
        request.input('type', sql.VarChar, type);
        request.input('user', sql.VarChar, user);
        request.input('name', sql.VarChar, name);
        request.input('detail', sql.NVarChar, comment);

        if(!callback) callback = function(){};

        request.execute('districtDaily-Comment', callback);
    });
}
exports.comment = comment;

var groupSocialData = function(socialData){
	var us = require('underscore')._;
	var returnObj = {}

	 returnObj.likes = us.where(socialData,{act: 'like'});
	 returnObj.comments = us.where(socialData,{act: 'comment'});

	return returnObj;
}

var getSocial = function(spId, type, callback){
	var sql = require('mssql');
	var sqlUtil = require('../models/sqlUtil');

	var connection = new sql.Connection(sqlUtil.getConfig('Forms'), function(err) {
        var request = new sql.Request(connection);

        request.input('spId', sql.Int, spId);
        request.input('type', sql.VarChar, type);

        if(!callback) callback = function(){};

        request.execute('districtDaily-getSocial', function(err, socialData){
        	callback(err, groupSocialData(socialData[0]) );
        });
    });
}
exports.getSocial = getSocial;