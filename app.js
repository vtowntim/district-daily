 console.log(new Date().toString());
 /**                   
  * Module dependencies          .
  */      

 var express = require('express');

 var routes = require('./routes');
 var ping = require('./routes/ping');
 var api = require('./routes/api');
 var records = require('./routes/records');
 var guide = require('./routes/guide');
 var scheduledContent = require('./routes/scheduledContent');

 var http = require('http');
 var path = require('path');


 var app = express();

 global.APPDIR = __dirname; 

 app.locals.moment = require('moment');   
 app.locals._ = require("underscore");       

 // all environments
 app.set('port', process.env.PORT || 3000);
 app.set('views', path.join(__dirname, 'views'));
 app.set('view engine', 'jade');
 app.use(express.favicon());
 app.use(express.logger('dev'));
 app.use(express.json());
 app.use(express.urlencoded());
 app.use(express.methodOverride());
 app.use(express.cookieParser());
 app.use(app.router);
 // app.use(require('less-middleware')({
 //     src: path.join(__dirname, 'public')
 // }));
 app.use(express.static(path.join(__dirname, 'public')));

 app.enable('trust proxy'); 

 // development only
 //if ('development' == app.get('env')) {
 app.use(express.errorHandler());
 //}           

app.get('/', records.list);
app.get('/genList', records.genList);
app.get('/getUser', records.getUser);

// app.get('/ping', ping.ping);
// app.get('/ping10', ping.TenReview);

app.get('/4U70M473D-5Y573M', ping.ping);
app.get('/list', records.list);

// app.get('/email', records.emailList);
// app.get('/announcementMail', records.announcement);
// app.get('/announcement', records.announcementView);


app.get('/updateEvents', records.updateEvents);
app.get('/events', records.eventList);              
app.get('/events2', records.makeEventList);  

app.get('/app', routes.index);
app.get('/app/sharepointImport', records.sharepointImport);

app.get('/style', guide.style);
app.get('/policy', guide.policy);

app.get('/scheduledContent', scheduledContent.index);
app.get('/api/searchQuotes', api.searchQuotes);
app.post('/api/scheduledContent', api.saveContent);
app.get('/api/scheduledContent', api.getContent);

app.get('/usernames', api.usernames);     

app.post('/api/like', api.like);
app.get('/api/list', api.getList);
app.post('/api/comment', api.comment);
app.get('/api/social', api.getSocial);   


app.get(/^\/(20\d\d\/[0-1]\d\/[0-3]\d)\/F([\d]+)-?([0-9]*\.[0-9]*)?\/?(attachments$|post$|data$|dataDownload$)?\/?/, records.getFocus);
app.get(/^\/(20\d\d\/[0-1]\d\/[0-3]\d)\/([\d]+)\/?(attachments$|post$|data$|dataDownload$)?\/?/, records.getRecord);
app.get(/^\/(20\d\d\/[0-1]\d\/[0-3]\d)\/E([\d]+)-?([0-9]*\.[0-9]*)?\/?(attachments$|post$|data$|dataDownload$)?\/?/, records.getEvent);
app.get(/^\/(20\d\d\/[0-1]\d\/[0-3]\d)\/?/, records.getList);

 console.log(process.env.PORT);

 // app.get('/testData', routes.testData);
 // app.get('/users', user.list);

 http.createServer(app).listen(app.get('port'), function() {
     console.log('########## Express server listening on port ' + app.get('port'));
 });