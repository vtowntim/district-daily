if (typeof console != "undefined") console.clear();

$.ajaxSetup({
    timeout: 2000 //Time in milliseconds
});

throttleMinutes = 0;

apiTools = {};

apiTools.cacheNameFromObj = function(object){
	return JSON.stringify(object).replace(/\W|(from)|where|select|order|like|ds|by|mode|e/gi,'');
}

apiTools.cacheSet = function(itemName, value){
	itemName = typeof(itemName) != 'string' ? apiTools.cacheNameFromObj(itemName) : itemName;

	if (typeof(Storage) === "undefined") return false;

	localStorage.setItem(itemName, JSON.stringify(value));
	localStorage.setItem(itemName+'_TIME', moment().toJSON());

	if (typeof console != "undefined") console.log(itemName + ' Saved to Cache');
}

apiTools.cacheGet = function(itemName, throttleMins){
	itemName = typeof(itemName) != 'string' ? apiTools.cacheNameFromObj(itemName) : itemName;
	if (typeof throttleMins == 'undefined') throttleMins = 10;

	if (typeof(Storage) === "undefined") return false;

	var item = localStorage.getItem(itemName);
	if (!item || !item) return false;

	var itemTime = localStorage.getItem(itemName+'_TIME');
	if (moment().diff(itemTime,'minutes') >= throttleMins ) return false;

	if (typeof console != "undefined") console.log(itemName + ' Received from Cache -- Age: ' + moment(itemTime).fromNow(1) );

	if (item == 'undefined') return false;

	return JSON.parse(item);
}

formatPhone = function(number){
	if (number === 'undefined') return number;
	number = $.trim(number)
    	.replace(/(\(?(559)\)?\s?)?624-?/gi,'')
    	.replace(/\(559\)\s?/gi,'')
    	.replace(/^9([0-9]{7})$/gi,'$1')
    	.replace(/^559([0-9]{7})$/gi,'$1')
    	.replace(/([0-9]{3})([0-9]{4})/gi,'$1-$2');
    if (number.length == 4) number = 'x' + number;
	return number;
}

function makeMap(myLocation,size){
	size = size || '300x378';
	var zoom = size == '75x75' ? '15' : '16';
	locations = {
		'The LifeStyle Center': '5105 W Cypress Ave Visalia, CA',
		'Kaweah Delta Medical Center': '400 W Mineral King Ave Visalia, CA',
		'Support Services Building': '520 W Mineral King Ave',
		'Hospice': '900 W Oak Ave, Visalia, CA',
		'Kaweah Delta Mental Hlth Hosp.': '1100 S Akers St, Visalia, CA',
		'Kaweah Delta Rehabilitation': '840 S Akers St, Visalia, CA',
		'Sequoia Imaging Center': '4949 W Cypress Ave, Visalia, CA',
		'Sequoia Regional Cancer Care': '4945 W Cypress Ave, Visalia, CA',
		'Sequoia Prompt Care': '1110 S Ben Maddox Way, Visalia, CA',
		'Exeter Health Clinic': '1014 San Juan Ave Exeter, CA',
		'Lindsay Health Clinic': '839 North Sequoia Avenue Lindsay, CA',
		'Woodlake Health Clinic': '180 E. Antelope Woodlake, CA',
		'Prime Infusion Network': '602 W Willow St, Visalia, CA'
	};

	if (locations[myLocation]){
		var loc = escape(locations[myLocation]);
	}else{
		var loc = escape(locations['Kaweah Delta Medical Center']);
	}
	var marker = size == '75x75' ? '' : '&markers=color:red|'+loc ;
	return 'http://maps.googleapis.com/maps/api/staticmap?center='+ loc +'&zoom='+zoom+'&size='+size+'&sensor=false&visual_refresh=true' + marker;
};


var apiURL = 'db/api.aspx?callback=?';



var vm = {
	appState: ko.observable(),
	loadCount: ko.observable(0),
	user: ko.observable({
		username: ko.observable(cookie.get('username')),
		userFullName: ko.observable(cookie.get('userFullName')),
		password: ko.observable(),
		status: ko.observable(false),
		groups: ko.observable(cookie.get('groups'))
	}),
	seltdContact: ko.observable({
		studentID: ko.observable(),
		LName: ko.observable(),
		FName: ko.observable(),
		SDate: ko.observable(),
		EDate: ko.observable(),
		School: ko.observable(),
		STypeID: ko.observable(),
		SpecID: ko.observable()
	}),
	student: {
		specialtyTypes: ko.observable(),
		studentTypes: ko.observable()
	},
	resident: {
		rotations: ko.observable()
	},
	seltdType: ko.observable(),
	search: ko.observable(),
	lastSearch: ko.observable(),
	windowScrolled: ko.observable(false),
	scrollY: ko.observable(),
	hashHistory: ko.observableArray(),
	loadingContacts: ko.observableArray(),
	contacts: ko.observableArray([]),
	zones: {
		ISS: ko.observable(false),
		editStudents: ko.observable(false),
		editResidents: ko.observable(false)
	}
}


ko.bindingHandlers.searchResultHighlight = {
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var valueUnwrapped = ko.unwrap(valueAccessor());
        var searchText = vm.search();
        var rex = new RegExp('('+searchText+')', 'gi');
        var name;
        if (valueUnwrapped.l){
        	name = valueUnwrapped.f + ' <strong>'+ valueUnwrapped.l +'</strong>';
        }else{
        	name = valueUnwrapped.t;
        }
        if (searchText && searchText.length > 1) name = name.replace(rex, '<span class="highlight">$1</span>');
        $(element).html(name);
    }
};

ko.bindingHandlers.inputPhone = {
	init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
		$(element).mask("(999) 999-9999");

		var underlyingObservable = valueAccessor();

		var interceptor = ko.computed({
			read: function () {
			// this function does get called, but it's return value is not used as the value of the textbox.
			// the raw value from the underlyingObservable, or the actual value the user entered is used instead, no   
			// dollar sign added. It seems like this read function is completely useless, and isn't used at all
			return underlyingObservable();
			},

			write: function (newValue) {
			var current = underlyingObservable()

			if (newValue !== current) {
			  // for some reason, if a user enters 20.00000 for example, the value written to the observable
			  // is 20, but the original value they entered (20.00000) is still shown in the text box.
			  underlyingObservable(newValue);
			} else {
			  if (newValue !== current.toString())
			    underlyingObservable.valueHasMutated();
			}
			}
			});

		ko.applyBindingsToNode(element, { value: interceptor });


	}
}

ko.bindingHandlers.contactPhone = {
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var valueUnwrapped = ko.unwrap(valueAccessor());
        var myPhone = valueUnwrapped.phone ? valueUnwrapped.phone : valueUnwrapped.deptPhone;
        myPhone = valueUnwrapped.Other_Phone ? valueUnwrapped.Other_Phone : myPhone;
        myPhone = formatPhone(myPhone);
        $(element).html(myPhone);
    }
};

ko.bindingHandlers.startDate = {
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var startDateObj = ko.unwrap(valueAccessor());
        var myDate = moment(startDateObj.date);

        switch(startDateObj.format){
			case 'ago':
				myDate = myDate.fromNow();
			break;
			case 'relative':
				myDate = myDate.fromNow(true);
			break;
			case 'custom':
				myDate = myDate.format(startDateObj.customFormat);
			break;
			default:
				myDate = myDate.format("MMMM Do, YYYY");
		}

        
        $(element).text(myDate);
    }
};

ko.bindingHandlers.kdPeople = {
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var valueUnwrapped = ko.unwrap(valueAccessor());
        var navObj = {
        	verb: 'search',
        	adj: 'all',
        	noun: 'a',
        	caption: 'A'
        };

        _.extend(navObj, valueUnwrapped);
        
        $(element).html('<a href="#'+ navObj.verb +'/'+ navObj.adj +'/'+ encodeURI(navObj.noun) +'">'+ navObj.caption +'</a>');
    }
};

ko.punches.interpolationMarkup.enable();
ko.punches.textFilter.enableForBinding("text");

ko.applyBindings(vm);

//	Sort by FName:
//  vm.contacts(_.sortBy(vm.contacts(), 'FName'))

getID = function(data){
	switch(data.type){
		case 'physician':
			return data.mdNum.split('/')[0];
		break;
		default:
			return data.id;
	}
}

changeType = function(type){
	type = hashToNavObj().adj == type ? null : type;
	if (type){
		navObjToHash({verb:'search',adj: type, noun:null});
	}else{
		navObjToHash(null);
	}
	$('#search').focus();
}

viewLoginBox = function(){
	navObjToHash({verb:'login'});
	setTimeout(function(){
		$('#username').focus();
	},5);
}

login = function(){
	vm.user().status('Logging In');
	retrieveData({
		mode: 'login',
		username: vm.user().username(),
		password: vm.user().password()
	}, function(data){
		if (data[0].auth) { 
			vm.user().userFullName(data[1].Name);

			if (data[0].groups){
				vm.user().groups(data[0].groups);
			}

			loggedIn();
		}

	})
}

loginAuto = function(){
	retrieveData({mode:'detectID'}, function(data){
		if(data.user != 'PUBLIC'){
			vm.user().username(data.user);
			vm.user().password('AU70L0GUTL0GIN0L0G!N');
			login();
		}
	}, 'http://apps.kdhcd.org/auth/api.aspx?callback=?&');
}

loggedIn = function(doNotSetCookie){
	vm.user().status('Logged In');
	vm.user().password(null);

	processPermissions();

	if (!doNotSetCookie){
		cookie.set('userFullName', vm.user().userFullName(),{
			expires: moment().add('minutes', 90).toDate() // expires in 90 minutes
		});
		cookie.set('username', vm.user().username(), {
		   expires: moment().add('minutes', 90).toDate() // expires in 90 minutes
		});
		cookie.set('groups', vm.user().groups(), {
		   expires: moment().add('minutes', 90).toDate() // expires in 90 minutes
		});
		navObjToHash({});
	}

}

logOut = function(confirmed){

	if(confirm('Are you sure you want to log out?')){
		cookie.empty();
		vm.user().username(null);
		vm.user().status(null);
		navObjToHash({});

		$.each(vm.zones,function(k,v){
			vm.zones[k](false);
		});

	}
}

permCheck = function(adGroup){
	var ckRegExp = new RegExp('('+ adGroup +'.*?),','g');
	return vm.user().groups().match(ckRegExp) ? true : false;
}

processPermissions = function(){
	if (permCheck('KDPeople Residents Edit')) vm.zones.editResidents(true);
	if (permCheck('KDPeople Students Edit')) vm.zones.editStudents(true);
	if (permCheck('Information Systems Services')) vm.zones.ISS(true);
}

retrieveData = function(dataObj, callback, dataURL){

	dataURL = dataURL || apiURL;

	// var cached = apiTools.cacheGet(dataObj, throttleMinutes);
	// if (cached){
	// 	if (callback) callback(cached);
	// 	return false;
	// }

	vm.loadCount( vm.loadCount() + 1 );

	$.getJSON(dataURL, dataObj,
		function(data){
			vm.loadCount( vm.loadCount() - 1 );
			if (callback) callback(data);
			//apiTools.cacheSet(dataObj, data);
		});

}

addToResults = function(newData){
	var myArray = vm.loadingContacts();
	ko.utils.arrayPushAll(myArray, newData);
	vm.loadingContacts.sort(function(left, right) {
		if (left.lName == hashToNavObj().noun) return -1;
		return left.lName == right.lName ? 0 : (left.lName < right.lName ? -1 : 1)
	})
	if (vm.loadCount() == 0){
		vm.contacts(vm.loadingContacts());
	};
}

letter = function(myLetter){
	vm.search(myLetter);
	doSearch();
	$('#search').select();
}

doSearch = function(){
	//console.log('search');
	navObjToHash({verb:'search', noun: vm.search()});
	//$('#search').select();
}

throttledSearch = _.debounce(doSearch, 700);



getContacts = function(type, term){

	//If we're re-doing the same search //NOT ANYMORE: and the user isn't logged in, // just keep the last results
	if (vm.lastSearch() == type + term ) { //&& !vm.user().status()
		restoreScrollLocation();
		return false;
	};
	vm.lastSearch(type + term);

	vm.contacts([]); vm.loadingContacts([]);

	type = type == 'all' ? null : type;

	// if the type is in the array, the default search term is % otherwise its A
	// this makes it so the default view for res and stu returns all people instead of A's...
	var defaultTerm = ['resident','student'].indexOf(type) + 1 ? '%' : 'A'

	term = term || defaultTerm;
	
	var where = '', deptWhere = '';
	switch(term.length){
		case 0:
			if (type != 'newHires') return false;
		break;
		case 1:
			where = "lName like '" +term + "%'";
			deptWhere = "DeptName like '"+ term +"%' "; //or Alt_Names like '"+ term.replace(/\s/g,'%') +"%' or Alt_Names like '%,"+ term.replace(/\s/g,'%') +"%' ";
		break;
		default:
			where = "fullName like '%" +term.replace(/\s/g,'%') + "%' or username like '%" +term.replace(/\s/g,'%') + "%'  or lName + ' ' + fName + ' ' + lName like '%" +term.replace(/\s/g,'%') + "%' or dept like '%" +term + "%'";
			deptWhere = "DeptName like '%"+ term.replace(/\s/g,'%') +"%' or location like '%"+ term.replace(/\s/g,'%') +"%' or id = \'" + term.replace(/\s/g,'%') + "\' " //or Alt_Names like '%"+ term.replace(/\s/g,'%') +"%'";
	}

	if (term.match(/^[0-9]{3,5}$/)){
	// //	vm.searchType('Doctor');
	 	where = "mdNum like '"+ term.replace(/^00/,'') +"%'";
	 }

	if (term.match(/^\[.*\]$/)){
	//	vm.searchType('Department');
		where = "DeptName = '"+ term.replace(/\[|\]/g,'') +"%'";
	}

	if (type == 'newHires'){
		var dataObj = {
				mode:'search',
				sp: 'employee.spNewHires'
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}


	if (!type || type == 'department'){
		var dataObj = {
				mode:'select',
				from: 'employee.View_dir_deptartment',
				where: "where "+ deptWhere +" order by DeptName"	
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}


	if (!type || type == 'student'){
		var dataObj = {
				mode:'search',
				sp: 'student.sp_view_dr_search',
				term: term.replace(/\s/g,'%'),
				flag: term.length == 1 ? 1 : 0
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}

	if (!type || type == 'resident'){
		var dataObj = {
				mode:'search',
				sp: 'resident.sp_view_dr_search',
				term: term.replace(/\s/g,'%'),
				flag: term.length == 1 ? 1 : 0
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}


	if (!type || type == 'employee'){
		var dataObj = {
				mode:'search',
				sp: 'employee.sp_view_dr_search',
				term: term.replace(/\s/g,'%'),
				flag: term.length == 1 ? 1 : 0
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}

	
	if (!type || type == 'physician'){
		var dataObj = {
				mode:'search',
				sp: 'physician.sp_view_dr_search',
				term: term.replace(/\s/g,'%'),
				flag: term.length == 1 ? 1 : 0
			};

		retrieveData(dataObj, function(data){
			addToResults(data);
		});
	}

	//////////////////////////////////////////////////////////

	// if (!type || type == 'student'){
	// 	var dataObj = {
	// 		mode: 'select',
	// 		from: 'student.View_dir',
	// 		select: '*',
	// 		where: "where "+ where +" order by lName, fName"
	// 		};

	// 	retrieveData(dataObj, function(data){
	// 		addToResults(data);
	// 	});
	// }

	// if (!type || type == 'resident'){
	// 	var dataObj = {
	// 		mode: 'select',
	// 		from: 'resident.View_dir',
	// 		select: '*',
	// 		where: "where "+ where +" order by lName, fName"
	// 		};

	// 	retrieveData(dataObj, function(data){
	// 		addToResults(data);
	// 	});
	// }

	// if (!type || type == 'employee'){
	// 	var dataObj = {
	// 			mode:'select',
	// 			select: '*',
	// 			from: 'employee.View_dir',
	// 			where: "where "+ where +" order by lName, fName"
	// 		};

	// 	retrieveData(dataObj, function(data){
	// 		addToResults(data);
	// 	});
	// }


	// if (!type || type == 'physician'){
	// 	var dataObj = {
	// 			mode:'select',
	// 			select: '*',
	// 			from: 'physician.View_dir',
	// 			where: "where "+ where +" order by lName, fName"
	// 		};

	// 	retrieveData(dataObj, function(data){
	// 		addToResults(data);
	// 	});
	// }

	
}

changeTitle = function(newTitle){
	newTitle = newTitle ? newTitle + ' - ' : '';
	document.title = newTitle + 'KDPeople';
}


getRotations = function(){

	retrieveData({
		mode: 'select',
		from: '[resident].[Tbl_Rotation_List]'
	},function(data){
		var sortedData = _.sortBy(data, function(e){return e.type;});
		vm.resident.rotations(sortedData);
	})

}

getSpecialties = function(){

	retrieveData({
		mode: 'select',
		from: '[student].[Tbl_Specialty_List]'
	},function(data){
		var sortedData = _.sortBy(data, function(e){return e.specialty;});
		vm.student.specialtyTypes(sortedData);
	})

}

getStudentTypes = function(){

	retrieveData({
		mode: 'select',
		from: '[student].[Tbl_StudentType_List]'
	},function(data){
		var sortedData = _.sortBy(data, function(e){return e.specialty;});
		vm.student.studentTypes(sortedData);
	})

}

showRewards = function(){
	$('#rewardsBtn').toggleClass('open');
	$('#rewards').toggle();
}

loadRecogPrefs = function(empID){
	empID = empID || vm.seltdContact().EmpID();

	retrieveData({
		mode:'recogPrefs',
		empID:empID
	},function(data){

		if(!data.length) return false;

		var rewardObj = _.groupBy(data,'reward_type')

		_.each(rewardObj, function(v,k,l){
			l[k] = _.pluck(v, 'reward').join(', ');
		})

		rewardObj = _.pairs(rewardObj);

		vm.seltdContact().rewards = ko.observableArray(rewardObj);
		vm.seltdContact.valueHasMutated();

	});
}

loadPriviledges = function(){
	retrieveData({
		mode:	'MDPriviledges',
		mdID:	vm.seltdContact().id()
	},function(data){

		data = _.toArray( _.groupBy( _.sortBy(data, 'Priv_Group') , 'Priv_Group') );

		vm.seltdContact().priviledges = ko.observableArray(data);
		vm.seltdContact.valueHasMutated();
	})
}

loadCallPrefs = function(drNum, callback){
	retrieveData({
		mode:'callPrefs',
		doctor_number:drNum
	},function(data){
		var culledData = [];
		$.each(data,function(k,v){
			culledData.push({
				order: data[k].Call_Order,
				type: data[k].Call_Type,
				details: data[k].Call_Details,
				number: data[k][data[k].Call_Field_Name]
			});
		})
		if(callback)callback(culledData);
	})
}

loadContact = function(navObj, callback){
	var id = navObj.noun;
	var type = navObj.adj;

	var payload;

	//console.log(type);

	switch(type){
		case 'student':
			payload = {
				mode: 'select',
				from: 'student.View_dir',
				where: 'where id = ' + id
			}
		break;
		case 'resident':
			payload = {
				mode: 'select',
				from: 'resident.View_dir',
				where: 'where id = ' + id
			}
		break;
		case 'employee':
			payload = {
				mode: 'select',
				from: 'employee.View_dir_full',
				where: 'where [id] = \'' + id + '\''
			}
		break;
		case 'department':
			payload = {
				mode: 'select',
				from: 'employee.View_dir_deptartment',
				where: 'where [id] = ' + id
			}
		break;
		case 'physician':
			payload = {
				mode: 'select',
				from: 'physician.View_dir',
				where: 'where mdNum like \'' + id + '%\''
			}
		break;
		default:
			return false;
	}

	if(id=='new'){
		payload.where = '';
		payload.select = 'top 1 *';
		callback({
			id: null,
			type: type,
			lName: null,
			fName: null,
			mName: null,
			fullName: null,
			dept: null,
			phone: null,
			deptPhone: null,
			username: null,
			SDate: null,
			EDate: null,
			School: null,
			STypeID: null,
			SpecID: null
		});
		return false;
	}

	retrieveData(payload, function(data){
		data = data[0];
		if(id=='new'){
			$.each(data,function(k,v){
				data[k] = null;
			})
			data.type = type;
		}
		if (data.SDate) data.SDate = moment(data.SDate).format('YYYY-MM-DD');
		if (data.EDate) data.EDate = moment(data.EDate).format('YYYY-MM-DD');
		if (callback) callback(data);
	});
}

openContact = function(contact){
	vm.seltdContact( ko.mapping.fromJS( contact ) );

	switch(contact.type){
		case 'department':
			changeTitle(contact.DeptName);
		break;
		default:
			changeTitle(contact.fName + ' ' + contact.lName);
	}

	if (contact.type == 'employee'){
		loadRecogPrefs(contact.EmpID);
	}

	delete vm.seltdContact().__ko_mapping__;
}

newPerson = function(type){
	location.hash = 'edit/'+ type +'/new';
}

removeFalsyProperties = function(myObject){
	$.each(myObject, function(k,v){
		if (!v){
			delete myObject[k];
		}
	});
	return myObject;
}


prepForDB = function(myString, joinLeft, joinRight){
	if (typeof myString == 'object') myString = joinLeft + myString.join( joinRight + ',' + joinLeft ) + joinRight;
	myString = myString.replace(/\'(undefined)?\'/g,'null');
	return myString
}

objToParameters = function(myObject){
	var parameters = [];
	$.each(myObject, function(k,v){
		parameters.push(k + '=' + '\'' + v + '\'');
	});
	return parameters.join(',')
}

makeSavePayload = function(myData){
	var mappedData, payload;


	switch(myData.type){
		case 'student':
			mappedData = {
				studentID: myData.id,
				LName: myData.lName,
				FName: myData.fName,
				SDate: myData.SDate,
				EDate: myData.EDate,
				School: myData.School,
				STypeID: myData.STypeID,
				SpecID: myData.SpecID
			};

			delete mappedData.studentID;

			payload = {
				id: myData.id ? myData.id : null,
				table: 'student.Masterdata',
				columnsValues: prepForDB(objToParameters(mappedData)),
				columns: prepForDB(_.keys(mappedData), '[', ']'),
				values: prepForDB(_.values(mappedData), '\'', '\''),
				photoTable: 'student.Slave_Image',
				photoColumns: 'studentID, studentimage',
				where: 'where [studentID] = ' + myData.id
			};
		break;

		case 'resident':
			mappedData = {
				studentID: myData.id,
				SDate: myData.SDate,
				EDate: myData.EDate,
				RotationID: myData.rotationID,
				Other_Phone: myData.Other_Phone
			};

			delete mappedData.studentID;

			payload = {
				id: myData.id ? myData.id : null,
				table: 'resident.Slave_Details',
				columnsValues: prepForDB(objToParameters(mappedData)),
				columns: prepForDB(_.keys(mappedData), '[', ']'),
				values: prepForDB(_.values(mappedData), '\'', '\''),
				where: 'where [EmpID] = ' + myData.id
			};
		break;
	}

	//console.log(myData.id);

	payload.mode = myData.id ? 'update' : 'insert';


	return payload;
}

saveContact = function(){
	var myData = ko.toJS(vm.seltdContact());
	
	var payload = makeSavePayload(myData);

	throttleMinutes = 0;

	retrieveData(payload, function(data){
		//console.log(data);
		var myID = payload.id ? payload.id : data.id;
		navObjToHash({verb: 'view', noun: myID});

		if (payload.photoTable){
			uploadPhoto(myID, payload.photoTable, payload.photoColumns);
		}
		

	})
}

deleteStudent = function(){

	var myData = ko.toJS(vm.seltdContact());

	if( !confirm('Are you sure you want to delete ' + myData.fName + ' ' + myData.lName + '?') ) return false;

	

	var payload = {
		table: 'student.Masterdata',
		column: 'studentID',
		value: myData.id,
		mode: 'delete'
	};

	retrieveData(payload, function(data){
		returnToSearch();
	})
}

selectPhoto = function(){
	$('#newPhoto').click();
}

selectedPhoto = function(me){
	var file = me.files[0];
    if (!file || !(file.type.indexOf('image/') + 1)) {
    	$('#profilePhoto').css({'background-image': 'url(http://apps.kdhcd.org/dir/new/img/noPhoto.jpg)'});
    	return false;
    };

    fr = new FileReader();
	fr.readAsDataURL(file);
	fr.onload = function(){
		$('#profilePhoto').css({'background-image': 'url('+ fr.result + ')' });
		};

}

uploadPhoto = function(id, table, columns){
	if (!$('#newPhoto').val()) { return false; }

	$('#newPhoto').upload(apiURL,{
		mode: 'upload',
		table: table,
		columns: columns,
		id: id
	}, function(){
		//location.reload();
	});
}

clearSeltdContact = function(){
	for(var index in vm.seltdContact() ) {
	   if(ko.isObservable(vm.seltdContact()[index])) {
	      vm.seltdContact()[index](null);
	   }
	}
}

editContact = function(){
	navObjToHash({verb: 'edit'});
}

backToSearch = function(){
	var navObj = hashToNavObj();

	if(navObj.verb == 'edit'){
		if (!confirm('Are you sure you want to close this contact without saving?')) return false;
	}

	// navObjToHash({
	// 	verb: 'search',
	// 	adj: hashToNavObj().adj,
	// 	noun: vm.search()
	// });

	returnToSearch();

	
}

returnToSearch = function(){

	// find in the hashHistory, the navObjs whose verb isn't view or edit
	var filteredHistory = _.filter(vm.hashHistory(), function(navObj){ return !(['view','edit','login'].indexOf(navObj.verb) + 1); });

	if(!filteredHistory.length){
		filteredHistory = hashToNavObj();
		filteredHistory.verb = 'search';
	}else{
		filteredHistory = _.last(filteredHistory);
	}


	navObjToHash(filteredHistory);
}

navObjToHash = function(navObj){

	navObj = navObj || {};

	if ( ! _.isEmpty(navObj) ){
		var currentNavObj = hashToNavObj();
		_.extend(currentNavObj, navObj);
		navObj = currentNavObj;
	}

	if (navObj.noun && !navObj.adj){
		navObj.adj = 'all';
	}

	var myHash = _.toArray(navObj).join('/');
	myHash = myHash == '//' ? '' : myHash;
	location.hash = myHash;
}

objArrayToCSV = function(objArray){
	var workingArray = [];

	workingArray.push( '"' + _.keys(objArray[0]).join('","') + '"' );

	$.each(objArray, function(k,v){
		workingArray.push( '"' + _.values(v).join('","') + '"' )
	})

	return workingArray.join('\r\n');
}

openCSV = function(csvData){
	var DataURI = 'data:text/csv;base64,' + btoa( objArrayToCSV(csvData) );
	//location.href =  DataURI;
	//$('<a>#</a>').attr('href', DataURI).attr('download','KDPeople.csv').appendTo('body').click();

	var navObj = hashToNavObj();

	var link = document.createElement('a');
    link.download = 'KDPeople - '+ navObj.adj +' - '+ navObj.noun +'.csv';
    link.href = DataURI;
    link.click();
}

downloadContacts = function(){
	openCSV(vm.contacts());
}

hashToNavObj = function(hash){
	hash = hash || location.hash;
	var hashArr = hash.match(/[^#/]*/g); //Break the sections into an array
	var navObj = _.object(['verb','adj','noun'], _.compact(hashArr)); //Convert to an object by mapping the parameter names to the array
	if (navObj.noun) {
		navObj.noun = decodeURI(navObj.noun);
		}
	return navObj;
}

saveScrollLocation = function(){
	vm.scrollY($(window).scrollTop());
}

restoreScrollLocation = function(){
	var scrollY = vm.scrollY();
	$(window).scrollTop(scrollY);
}

checkIfScrolled = function(){
	var scrollY = $(window).scrollTop();
	vm.windowScrolled( scrollY > 10 ? true : false );
}

$(window).scroll(checkIfScrolled);


$(window).hashchange( function(){

	var navObj = hashToNavObj();
	vm.hashHistory.push(navObj);

	if (!navObj.adj) vm.seltdType(null);

	switch(navObj.verb){
		case 'login':
			vm.appState('login');
		break;
		case 'edit':
			if (navObj.noun){
				loadContact(navObj, function(contact){
					openContact(contact);
					vm.appState('edit');
				})
			}
		break;
		case 'view':
			if (navObj.noun){

				loadContact(navObj, function(contact){



					if (contact.type == 'physician'){
						vm.seltdContact().callPrefs = [];
						loadCallPrefs(contact.id,function(callPrefs){
							vm.seltdContact().callPrefs = callPrefs;
							vm.seltdContact.valueHasMutated();
						})
					}

					openContact(contact);

					vm.appState('view');
				});

			}

		break;

		case 'search':
			vm.appState('');
			changeTitle( vm.search() );

			if (navObj.adj){
				if (navObj.noun || ['department','employee','resident','student','physician'].indexOf(navObj.adj) + 1 ){
					if (navObj.noun != 'all') vm.search(navObj.noun);
					vm.seltdType(navObj.adj);
					getContacts(navObj.adj, navObj.noun);
				}else{
					navObjToHash({adj: 'all', noun: navObj.adj});
				}
			}else{
				vm.seltdType(null);
				vm.contacts(null);
				$('#search').focus();
				//getContacts('all','');
			}

		break;

		default:
			changeTitle();
			vm.appState('newHires');
			getContacts('newHires', null);
	}

})


$('#mainContent').on('click', '#contacts li', function(){
	saveScrollLocation();
	navObjToHash({
		verb: 'view',
		adj: $(this).attr('data-type'),
		noun: $(this).attr('data-id')
	});
});


$('#search').keyup(function(e){
	$(this).change();
	throttledSearch();
	if (e.which == 13){
		doSearch();
	}
})



$(function(){

	if (cookie.get('username')){
		loggedIn(true);
	}else{
		loginAuto();
	}

	getRotations();
	getSpecialties();
	getStudentTypes();

	var navObj = hashToNavObj();
	if (navObj.verb == 'search'){
		vm.seltdType(navObj.adj);
	}

	if ( !(['view','login'].indexOf(navObj.verb)+1) ){
		$('#search').focus();
	}
	

	$(window).hashchange();

})





