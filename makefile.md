#KDPeople Makefile

##CSS
lessc css/style.less css/style.css -x

##JS
uglifyjs js/libs/*.js js/source.js -o js/scriptv01_2.js

##MOBILE JS
uglifyjs js/libs/*.js js/mobileSource.js -o js/mobilescriptv01.js

##JS Libs
uglifyjs libs/*.js -o libs.js

##Docs
markdowns docs -p

##GIT
git add .
git commit -m "MESSAGE"
git push -u origin master
      